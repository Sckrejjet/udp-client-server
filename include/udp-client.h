#ifndef UDP_CLIENT_H
#define UDP_CLIENT_H

#include <sys/socket.h>
#include <arpa/inet.h>
#include <string>

namespace JustUdp
{

class Client
{
public:
    explicit Client(const std::string &ip, int port);
    ~Client();
    bool init();
    int send(const char* data, size_t size);
    int receive(char* data, size_t size);

private:
    std::string serverIp;
    int serverPort;
    int sock;
    struct sockaddr_in serverAddr;
    socklen_t serverAddrLen;
};

}

#endif  // UDP_CLIENT_H
