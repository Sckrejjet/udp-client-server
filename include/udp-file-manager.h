#ifndef UDP_FILE_MANAGER_H
#define UDP_FILE_MANAGER_H

#include <stddef.h>
#include <stdint.h>
#include <vector>
#include <utility>
#include <map>
#include <algorithm>

#include "udp-packet.h"

namespace JustUdp
{

class FileStorage
{
public:
    using PairType = std::pair<bool, JustUdp::packet>;

    FileStorage(std::size_t size);

    void generatePackets(std::ifstream &f, std::size_t size, const std::string &id);

    bool hasPart(std::uint32_t seq_number);

    void addPart(const JustUdp::packet &pack);

    std::uint32_t seqTotal() const;

    bool empty() const;

    bool isFull() const;

    void calculateCrc();

    std::uint32_t getCrc() const;

    void shuffle();

    void inc();

    JustUdp::packet getPacket();

    bool allSent() const;

private:
    int packet_idx;
    std::vector<PairType> packets;
    std::uint32_t crc;
};


class FileManager
{
public:
    using StorageMapType = std::map<std::string, JustUdp::FileStorage>;

    FileManager();
    ~FileManager();

    bool loadFile(const std::string &fileName);

    bool empty() const;

    bool hasPackets() const;

    bool hasAnyPacketToSend() const;

    std::uint32_t addPacket(char* buf);

    bool hasStorage(const std::string &id) const;

    FileStorage& getStorage(const std::string &id);

    void confirmAck();

    JustUdp::packet getPacket(bool &ok);

private:
    StorageMapType storages;

    bool ack;
    std::size_t file_idx;
    StorageMapType::iterator currStorIt;
};

}  // namespace JustUdp

#endif  // UDP_FILE_MANAGER_H
