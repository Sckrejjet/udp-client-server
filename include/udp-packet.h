#ifndef UDP_PACKET_H
#define UDP_PACKET_H

#include <iostream>
#include <cstdint>
#include <cstring>
#include <type_traits>
#include <string>
#include <stdint.h>

namespace JustUdp
{

constexpr std::size_t MAX_PACKET_SIZE = 1472;
constexpr std::uint8_t ACK = 0;
constexpr std::uint8_t PUT = 1;

template <typename... ArgsT>
void massive_print(ArgsT&&... args)
{
    ((std::cout << std::dec << std::forward<ArgsT>(args) << std::endl), ...);
}

template <typename ValT>
inline void add_to_buffer(char* buf, ptrdiff_t &off, ValT value)
{
    if constexpr (std::is_pointer<ValT>::value)
        memcpy(buf + off, value, sizeof(value));
    else
        memcpy(buf + off, &value, sizeof(value));
    off += sizeof(value);
}

template <typename ValT>
inline void get_from_buffer(char* buf, ptrdiff_t &off, ValT& value)
{
    if constexpr (std::is_pointer<ValT>::value)
        memcpy(value, buf + off, sizeof(value));
    else
        memcpy(&value, buf + off, sizeof(value));
    off += sizeof(value);
}

template <typename... ArgsT>
inline void serialize(char* buf, ptrdiff_t &off, ArgsT&&... args)
{
    (add_to_buffer(buf, off, std::forward<ArgsT>(args)), ...);
}

template <typename... ArgsT>
inline void deserialize(char* buf, ptrdiff_t &off, ArgsT&&... args)
{
    (get_from_buffer(buf, off, std::forward<ArgsT>(args)), ...);
}

struct packet
{
    struct header
    {
        std::uint32_t seq_number;
        std::uint32_t seq_total;
        std::uint8_t type;
        char id[8]{};

        static constexpr size_t ID_SIZE = sizeof(id);

        // Header size
        static constexpr std::size_t SIZE =
            sizeof(seq_number) + sizeof(seq_total) + sizeof(type) + sizeof(id);

        void to_data(char* buf)
        {
            ptrdiff_t off = 0;
            serialize(buf, off, seq_number, seq_total, type, id);
        }

        void from_data(char* buf)
        {
            ptrdiff_t off = 0;
            deserialize(buf, off, seq_number, seq_total, type, id);
        }

        std::string id_str() const
        {
            return std::string(id, sizeof(h.id));
        }

        std::size_t id_num() const
        {
            std::size_t file_id{0};
            memcpy(&file_id, id, sizeof(file_id));
            return file_id;
        }
    };

    // Header
    header h;
    // Buffer size
    static constexpr std::size_t DATA_SIZE = MAX_PACKET_SIZE - header::SIZE;
    // Buffer
    char data[DATA_SIZE]{};

    void to_data(char* buf)
    {
        auto off = static_cast<ptrdiff_t>(header::SIZE);
        h.to_data(buf);
        memcpy(buf + off, data, DATA_SIZE);
    }

    void from_data(char* buf)
    {
        h.from_data(buf);
        buffer_from_data(buf);
    }

    void buffer_from_data(char* buf)
    {
        auto off = static_cast<ptrdiff_t>(header::SIZE);
        memcpy(data, buf + off, DATA_SIZE);
    }
};

}

#endif  // UDP_PACKET_H
