#ifndef UDP_SERVER_H
#define UDP_SERVER_H

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string>

namespace JustUdp
{

class Server
{
public:
    Server(int port);
    ~Server();

    bool init();
    int send(const char* data, size_t size);
    int receive(char* data, size_t size);

private:
    int m_port;
    int sock;
    struct sockaddr_in m_addr;
    struct sockaddr_in clientAddr;
    socklen_t clientAddrLen;
};

}

#endif  // UDP_SERVER_H
