#include <iostream>
#include <chrono>
#include <thread>
#include <vector>
#include <random>
#include <algorithm>

#include "udp-client.h"
#include "udp-packet.h"
#include "udp-file-manager.h"


enum class ClientState
{
    SEND_TO_SERVER,
    WAIT_FOR_ACK
};


int main()
{
    using TimePt = std::chrono::time_point<std::chrono::steady_clock>;

    // fisrs - client side crc
    // second - server side crc
    std::map<std::string, std::pair<std::uint32_t, ::uint32_t>> crcStorage;

    JustUdp::FileManager man;
    man.loadFile("../files/file1");
    man.loadFile("../files/file2");

    constexpr size_t SEND_INTERVAL = 20;
    constexpr double DEFAULT_TIMEOUT = SEND_INTERVAL/200.;

    char buffer[JustUdp::MAX_PACKET_SIZE];

    ClientState state = ClientState::SEND_TO_SERVER;
    JustUdp::Client client("127.0.0.1", 8080);
    if (!client.init())
    {
        std::cout << "Client was not initialized correctly..." << std::endl;
        return 1;
    }

    JustUdp::packet currPacket;

    TimePt cl_start;
    TimePt cl_end;
    while (true)
    {
        if (state == ClientState::SEND_TO_SERVER)
        {
            bool ok = false;
            currPacket = man.getPacket(ok);
            if (!ok)
            {
                std::cout << "Some error ocured" << std::endl;
                return 2;
            }
            std::cout << ">>> PUT - id: "
                      << currPacket.h.id_num()
                      << " seq_number: "
                      << currPacket.h.seq_number
                      << std::endl;
            currPacket.to_data(buffer);
            client.send(buffer, JustUdp::MAX_PACKET_SIZE);
            state = ClientState::WAIT_FOR_ACK;
            cl_start = std::chrono::steady_clock::now();
        }
        else if (state == ClientState::WAIT_FOR_ACK)
        {
            auto recvSize = client.receive(buffer, JustUdp::MAX_PACKET_SIZE);
            if (recvSize == JustUdp::MAX_PACKET_SIZE)
            {
                JustUdp::packet inPack;
                inPack.from_data(buffer);
                bool isAck = inPack.h.type == JustUdp::ACK;
                bool sameId = inPack.h.id_str() == currPacket.h.id_str();
                bool sameNum = inPack.h.seq_number == currPacket.h.seq_number;
                if (isAck && sameId && sameNum)
                {
                    std::cout << "<<< ACK - id: "
                              << inPack.h.id_num()
                              << " seq_number: "
                              << inPack.h.seq_number
                              << std::endl;
                    if (inPack.h.seq_total == currPacket.h.seq_total)
                    {
                        auto &stor = man.getStorage(inPack.h.id_str());
                        std::uint32_t inCrc = 0;
                        memcpy(&inCrc, inPack.data, sizeof(inCrc));
                        crcStorage[inPack.h.id_str()] = {inCrc, stor.getCrc()};
                        if (!man.hasAnyPacketToSend())
                        {
                            break;
                        }
                    }
                    man.confirmAck();
                }
                state = ClientState::SEND_TO_SERVER;
            }

            cl_end = std::chrono::steady_clock::now();
            std::chrono::duration<double> timeout = cl_end - cl_start;

            if (timeout.count() > DEFAULT_TIMEOUT)
            {
                std::cout << "Timeout exceeded... Send again." << std::endl;
                state = ClientState::SEND_TO_SERVER;
            }
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(SEND_INTERVAL));
    }

    for (const auto crcById : crcStorage)
    {
        auto &crcPair = crcById.second;
        std::size_t file_id = 0;
        memcpy(&file_id, crcById.first.c_str(), sizeof(file_id));
        std::cout << "For file with id: " << file_id
                  << " Client's crc: " << crcPair.first
                  << " Server's crc: " << crcPair.second
                  << std::endl;
    }
    return 0;
}
