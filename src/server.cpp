#include <iostream>
#include <chrono>
#include <thread>
#include <random>
#include <algorithm>

#include "udp-server.h"
#include "udp-packet.h"
#include "udp-file-manager.h"

constexpr int PORT = 8080;

int main()
{
    JustUdp::FileManager man;

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> distrib(1, 6);
    JustUdp::Server server(PORT);
    if (!server.init())
    {
        std::cout << "Server was not initialized correctly..." << std::endl;
        return -1;
    }

    std::cout << "Server started on port: " << PORT << std::endl;

    char buffer[JustUdp::MAX_PACKET_SIZE]{};
    bool was_sent = true;
    while (true)
    {
        auto recvSize = server.receive(buffer, JustUdp::MAX_PACKET_SIZE);
        if (recvSize == JustUdp::MAX_PACKET_SIZE)
        {
            // Load incoming data from buffer to packet
            JustUdp::packet p_in;
            p_in.from_data(buffer);

            // Add packet to storage and prepare reply
            std::uint32_t seqTotal = man.addPacket(buffer);
            // Add a little bit of randomization to not respond somtime :)
            if (distrib(gen) > 2)
            {
                JustUdp::packet p_ack;
                p_ack.h.seq_number = p_in.h.seq_number;
                p_ack.h.seq_total = seqTotal;
                p_ack.h.type = JustUdp::ACK;
                memcpy(p_ack.h.id, p_in.h.id, sizeof(p_in.h.id));
                // If we have all packets with this id
                if (seqTotal == p_in.h.seq_total)
                {
                    auto &stor =  man.getStorage(p_in.h.id_str());
                    stor.calculateCrc();
                    std::uint32_t resCrc = stor.getCrc();
                    memcpy(p_ack.data, &resCrc, sizeof(resCrc));
                    std::size_t file_id = 0;
                    memcpy(&file_id, p_ack.h.id, sizeof(file_id));
                    // This message can be printed multiple times it slient
                    // didn't get ACK response from server
                    std::cout << "Packet with id: " << file_id << " completed. "
                              << "Result CRC: " << resCrc << std::endl;
                }
                p_ack.to_data(buffer);
                server.send(buffer, JustUdp::MAX_PACKET_SIZE);
            }
        }
    }
    return 0;
}
