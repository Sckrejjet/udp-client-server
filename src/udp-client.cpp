#include <iostream>
#include <cstring>
#include <unistd.h>

#include "udp-client.h"
#include "udp-packet.h"

namespace JustUdp
{

Client::Client(const std::string &ip, int port)
    : serverIp(ip)
    , serverPort(port)
{

}

Client::~Client()
{
    close(sock);
}

bool Client::init()
{
    if ( (sock = socket(AF_INET, SOCK_DGRAM | SOCK_CLOEXEC | SOCK_NONBLOCK, IPPROTO_UDP)) < 0 )
    {
        std::cout << "Client socket error..." << std::endl;
        close(sock);
        return false;
    }

    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(serverPort);
    serverAddr.sin_addr.s_addr = inet_addr(serverIp.c_str());

    return true;
}

int Client::send(const char* data, size_t size)
{
    return sendto(sock, data, size, 0, (struct sockaddr*)&serverAddr, sizeof(serverAddr));
}

int Client::receive(char* data, size_t size)
{
    serverAddrLen = sizeof(serverAddr);
    return recvfrom(sock, data, size, 0, (struct sockaddr*)&serverAddr, &serverAddrLen);
}

}
