#include <random>
#include <fstream>
#include <limits>

#include "udp-file-manager.h"

namespace JustUdp
{

namespace detail
{

class FileGuard
{
public:
    FileGuard(std::ifstream &file) : f(file) {}
    ~FileGuard() { f.close(); }

private:
    std::ifstream &f;
};

}  // namespace detail

FileStorage::FileStorage(std::size_t size)
    : packet_idx(-1)
    , crc(0)
{
    packets.resize(size, {false, {}});
}

void FileStorage::generatePackets(std::ifstream &f, std::size_t size,
                                  const std::string &id)
{
    for (int i = 0; i < size; ++i) {
        JustUdp::packet pack;
        pack.h.seq_number = i;
        pack.h.seq_total = size;
        pack.h.type = JustUdp::PUT;
        memcpy(pack.h.id, id.c_str(), sizeof(pack.h.id));
        f.read(pack.data, JustUdp::packet::DATA_SIZE);

        packets[i] = {true, pack};
    }
}

bool FileStorage::hasPart(std::uint32_t seq_number)
{
    return packets[seq_number].first;
}

void FileStorage::addPart(const JustUdp::packet &pack)
{
    std::uint32_t idx = pack.h.seq_number;
    packets[idx].first = true;
    packets[idx].second = pack;
}

std::uint32_t FileStorage::seqTotal() const
{
    // We can probably just increase some sort of a counter on every
    // succesfull addPart() call. But for now it is much safer to rely on
    // the real container. (of cource this way is less effective)
    auto pred = [](const PairType &p) { return p.first; };
    const auto ttl_cnt = std::count_if(packets.begin(), packets.end(), pred);
    return static_cast<std::uint32_t>(ttl_cnt);
}

bool FileStorage::empty() const
{
    return packets.empty();
}

bool FileStorage::isFull() const
{
    return packets.size() == seqTotal();
}

void FileStorage::calculateCrc()
{
    crc = ~0;
    const std::size_t n_packets = packets.size();
    for (int pack_idx = 0; pack_idx < n_packets; ++pack_idx)
    {
        const auto &pack = packets[pack_idx].second;

        for (int i = 0; i < JustUdp::packet::DATA_SIZE; ++i)
        {
            crc ^= pack.data[i];
            for (int k = 0; k < 8; ++k)
            {
                crc = crc & 1 ? (crc >> 1) ^ 0x82f63b78 : crc >> 1;
            }
        }
    }
}

std::uint32_t FileStorage::getCrc() const
{
    return crc;
}

void FileStorage::shuffle()
{
    // Make sure we get crc before shuffle!
    std::random_device rd;
    std::mt19937 g(rd());
    std::shuffle(packets.begin(), packets.end(), g);
}

void FileStorage::inc()
{
    int p_size = static_cast<int>(packets.size());
    if ( packet_idx < (p_size - 1) )
    {
        ++packet_idx;
    }
}

JustUdp::packet FileStorage::getPacket()
{
    return packets[packet_idx].second;
}

bool FileStorage::allSent() const
{
    return packet_idx == packets.size() - 1;
}






FileManager::FileManager()
    : ack(true) // To perform first send
    , file_idx(0)
    , currStorIt(storages.begin())
{

}

FileManager::~FileManager() {}

bool FileManager::loadFile(const std::string &fileName)
{
    std::ifstream file;
    // To automatically close file in any case
    detail::FileGuard gGuard(file);

    file.open(fileName, std::ios::in | std::ios::binary);
    if (!file.is_open())
    {
        std::cout << "Can not open file: " << fileName << std::endl;
        return false;
    }

    file.ignore( std::numeric_limits<std::streamsize>::max() );
    std::streamsize ttlSize = file.gcount();
    if (ttlSize == 0)
    {
        std::cout << "The file is empty: " << fileName << std::endl;
        return false;
    }

    file.clear();
    file.seekg(0, std::ios::beg);

    std::size_t storCount = ttlSize / JustUdp::packet::DATA_SIZE;
    // Check if we need to add extra storage for file ending if it is less
    // than JustUdp::packet::DATA_SIZE
    if (ttlSize % JustUdp::packet::DATA_SIZE != 0)
    {
        ++storCount;
    }

    ++file_idx;
    JustUdp::packet::header hd;
    memcpy(hd.id, &file_idx, sizeof(file_idx));

    std::string id{hd.id_str()};
    char data[10]{};
    if (!hasStorage(id))
    {
        storages.emplace(id, storCount);
    }
    auto &stor = storages.at(id);
    stor.generatePackets(file, storCount, id);
    stor.calculateCrc();
    stor.shuffle();

    return true;
}

bool FileManager::empty() const
{
    return storages.empty();
}

bool FileManager::hasPackets() const
{
    if (storages.empty()) return false;

    bool has = false;
    for (const auto& it : storages)
    {
        has |= (it.second.seqTotal() > 0);
    }

    return has;
}

bool FileManager::hasAnyPacketToSend() const
{
    if (storages.empty()) return false;

    bool hasToSend = false;
    for (const auto& it : storages)
    {
        hasToSend |= (!it.second.allSent());
    }
    return hasToSend;
}

std::uint32_t FileManager::addPacket(char* buf)
{
    JustUdp::packet pack;
    pack.from_data(buf);
    auto p_id = pack.h.id_str();
    // Check if we have storage with such id. If not - create one.
    if ( !hasStorage(p_id) )
    {
        storages.emplace(p_id, pack.h.seq_total);
    }
    auto &storage = storages.at(p_id);
    if (storage.hasPart(pack.h.seq_number) == false)
    {
        storage.addPart(pack);
    }
    return storage.seqTotal();
}

bool FileManager::hasStorage(const std::string &id) const
{
    return storages.find(id) != storages.end();
}

FileStorage& FileManager::getStorage(const std::string &id)
{
    return storages.at(id);
}

void FileManager::confirmAck()
{
    ack = true;
}

JustUdp::packet FileManager::getPacket(bool &ok)
{
    if (storages.empty())
    {
        ok = false;
        return JustUdp::packet();
    }

    // We must use 'ack' here to send last existing packet because
    // hasAnyPacketToSend() return true as soon as packet_idx got max value
    // in the last FileStorage
    // Actually this logic is super bad and needs to be refactored...
    if (ack && !hasAnyPacketToSend())
    {
        ok = false;
        return JustUdp::packet();
    }
    // Probably we don't need previous extra checks because we must check them
    // in the main program. But let it be "super safe" for now...

    // If last packet was accepted by server we can send next
    if (ack)
    {
        int tryCount = 0;
        while (tryCount < storages.size())
        {
            // Increment iterator to "randomize" file selection
            currStorIt++;
            if (currStorIt == storages.end())
            {
                currStorIt = storages.begin();
            }

            auto &stor = currStorIt->second;
            if ( stor.seqTotal() > 0 && !stor.allSent() )
            {
                // Reset ack status before packet will be accepted
                ack = false;
                ok = true;
                stor.inc();
                return stor.getPacket();
            }
            ++tryCount;
        }

        // Return empty packet if iteration count exceeded
        ok = false;
        return JustUdp::packet();
    }
    else
    {
        ok = true;
        return currStorIt->second.getPacket();
    }
}

}  // namespace JustUdp
