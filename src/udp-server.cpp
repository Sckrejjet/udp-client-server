#include "udp-server.h"
#include "udp-packet.h"

#include <iostream>
#include <unistd.h>

namespace JustUdp
{

Server::Server(int port) : m_port(port) {}

Server::~Server()
{
    close(sock);
}

bool Server::init()
{
    if ( (sock = socket(AF_INET, SOCK_DGRAM | SOCK_CLOEXEC | SOCK_NONBLOCK, 0)) < 0 ) {
        std::cout << "Server socket error..." << std::endl;
        return false;
    }

    m_addr.sin_family = AF_INET;
    m_addr.sin_port = htons(m_port);
    m_addr.sin_addr.s_addr = INADDR_ANY;

    if ( (bind(sock, (struct sockaddr*)&m_addr, sizeof(m_addr))) < 0 ) {
        std::cout << "Server bind error..." << std::endl;
        close(sock);
        return false;
    }

    return true;
}

int Server::send(const char* data, size_t size)
{
    return sendto(sock, data, size, 0, (struct sockaddr*)&clientAddr, clientAddrLen);
}

int Server::receive(char* data, size_t size)
{
    clientAddrLen = sizeof(clientAddr);
    return recvfrom(sock, data, size, 0, (struct sockaddr*)&clientAddr, &clientAddrLen);
}

}
